## APPRISE REPORTS
------------------

Application support for analytical reports.

## Getting Started

---

This is an `apprise` fragment and adopts this standard structure:

* `lib` contains the code.
* `resources` contains assets, like translations and images.
* `lab` contains a minimal `apprise` application that tests and showcases the code.

Instructions on how to run the `lab` are in `lab/README.md`.
