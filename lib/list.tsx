import { useT } from 'apprise-frontend-core/intl/language'
import { useL } from 'apprise-frontend-core/intl/multilang'
import { usePreferences } from 'apprise-frontend-iam/user/preferences'
import { Button } from 'apprise-ui/button/button'
import { ChoiceBox } from 'apprise-ui/choicebox/choicebox'
import { classname } from 'apprise-ui/component/model'
import { Label, LabelRow } from 'apprise-ui/label/label'
import { Page } from 'apprise-ui/page/page'
import { SidebarContent } from 'apprise-ui/page/sidebar'
import { Counter } from 'apprise-ui/table/bar'
import { TextBox } from 'apprise-ui/textbox/textbox'
import { Titlebar } from 'apprise-ui/titlebar/titlebar'
import { Topbar } from 'apprise-ui/topbar/topbar'
import { DownloadIcon, LockIcon, SearchIcon } from 'apprise-ui/utils/icons'
import fastsort from 'fast-sort'
import { Fragment, useContext, useMemo, useState } from 'react'
import { MdOutlineMoreVert } from "react-icons/md"


import { DataviewCard } from './card'
import { AnalyticsDownloadStatsContext, AnalyticsEditContext } from './context'
import { useDownloadDrawer } from './downloadpanel'
import { LockedParamsLabels, RepositoryLabels } from './label'
import "./list.scss"
import { LockPanel } from './lockpanel'
import { useDataviewModel } from './model'
import { useAnalyticsOracle } from './oracle'
import { AnayticsPreferencs } from './preferences'
import { useAnalyticsProfile } from './profile'
import { newParam } from './routing'
import { useAnalyticsStore } from './store'
import { useViewParams } from './viewparams'



export const DataviewList = () => {

    const l = useL()
    const t = useT()

    const model = useDataviewModel()
    const store = useAnalyticsStore()
    const oracle = useAnalyticsOracle()

    const params = useViewParams()

    const { lockkit, addBtn } = useContext(AnalyticsEditContext)

    const { plural, noLockedParams } =  useAnalyticsProfile()

    const lockableParams = params.all().filter(p => p.lockable)
    const lockableInline = lockableParams.filter(p => p.lockable && p.lockedInline)
    const inlineLockables = lockableInline.length > 0
    const allInlined = inlineLockables && lockableInline.length === lockableParams.length

    const dataviews = store.all()

    const customViews = !!dataviews.find(r => !r.predefined)


    const preferences = usePreferences<AnayticsPreferencs>()


    const [filter, setFilter] = useState<string | undefined>(undefined)

    const initialMode = () => preferences.get().analytics?.displayMode === 'compact'

    const [compactMode, setCompactMode] = useState(initialMode)

    const [showAll, setShowall] = useState(true)

    const filteredDataviews = useMemo(() => {

        const filtered = dataviews
            .filter(view => filter ? model.textOf(view).toLowerCase().includes(filter.toLowerCase()) : view)
            .filter(view => showAll && customViews ? view : view.predefined)

        return fastsort(filtered).by([{ desc: r => r.predefined }, { asc: r => l(r.name) }])

        // eslint-disable-next-line
    }, [filter, dataviews, showAll])


    const flipMode = () => {

        const flipped = !compactMode

        setCompactMode(flipped)

        preferences.

            saveWith(

                prefs => prefs.analytics = { ...prefs.analytics, displayMode: flipped ? 'compact' : 'full' },
                config => config.quietly()

            )


    }


    const lockSuffixBtn = <Button className='lock-btn-suffix' tip={t('analytics.lock_btn_open_tip')} icon={<MdOutlineMoreVert size={18} color='dodgerblue' />} noLabel onClick={() => lockkit.openAt(newParam)} />

    const lockBtn = noLockedParams || <Button icon={<LockIcon />} onClick={() => lockkit.openSet(true)}>
        {t('analytics.lock_btn')}
    </Button>

    const downloadBtn = <DownloadButton />

    return <Page className='dataview-list'>

        <Titlebar title={t('analytics.list_title', { plural })}>
            <LabelRow>
                <RepositoryLabels />
                <LockedParamsLabels />
            </LabelRow>
        </Titlebar>

        <Topbar>


            <div className='locked-params'>

                {inlineLockables && <Label tip={t('analytics.locked_explainer')} noIcon title='lock:' />}

                {noLockedParams || <div className='locked-param-list'>

                    {inlineLockables && lockableInline.map(param => {

                        const Field = param.lockField!

                        return <Field renderMode='lock-inline' label={undefined} msg={undefined} className='locked-param' key={param.id} onChange={p => params.lock(param, p)}>
                            {params.lockOf(param)}
                        </Field>
                    })}

                    {allInlined || (inlineLockables ? lockSuffixBtn : lockBtn)}

                </div>}


            </div>

            {downloadBtn}

        </Topbar>

        <SidebarContent>
            {oracle.canManageAll() &&

                <Fragment>
                    {addBtn}
                    <br />
                </Fragment>

            }

            {lockBtn}
            {downloadBtn}

        </SidebarContent>


        <div className='list-ctrls'>

            <div className='ctrls-left'>
                {/* fill as required */}
            </div>

            <div className='ctrls-center'>

                <ChoiceBox style={{ marginRight: 15 }} horizontal value={!compactMode} onChange={flipMode}>
                    <ChoiceBox.Option value={false} title={t('analytics.list_mode_compact')} />
                </ChoiceBox>


                <div className='topbar-icon dataview-search' >

                    <Counter count={filteredDataviews.length} total={filteredDataviews.length} />

                    <TextBox className='dataview-search' noReadonly prefix={<SearchIcon />} placeholder={t('analytics.search_placeholder')} onChange={setFilter}>
                        {filter}
                    </TextBox>

                </div>

                {customViews &&

                    <ChoiceBox style={{ marginLeft: 20 }} horizontal value={showAll} onChange={() => setShowall(s => !s)}>
                        <ChoiceBox.Option value={false} title={t('analytics.list_exclude')} />
                    </ChoiceBox>}

            </div>

            <div className='ctrls-right'>
                {/* fill as required */}
            </div>
        </div>

        {filteredDataviews.map(view =>

            <DataviewCard key={view.id} dataview={view} displayMode={compactMode ? 'compact' : 'full'} />

        )}

        <LockPanel />



    </Page>
}


export const DownloadButton = () => {

    const t = useT()

    const { stats: download } = useContext(AnalyticsDownloadStatsContext).get()

    const profile =  useAnalyticsProfile()

    const downloadDrawer = useDownloadDrawer()

    if (profile.noDownload)
        return null

    const downloading = download?.status === 'inprogress' ? download.todo : 0

    return <Button className={classname(!!downloading && 'download-inprogress')} type='primary' icon={<DownloadIcon />} onClick={downloadDrawer.toggle}>
        {downloading ?

            <div className='apprise-row'>
                <div>{t('analytics.download_btn_inprogress')}</div>
                <div style={{ minWidth: 40 }}> ({`${downloading}`.padStart(2, '0')})</div>
            </div>
            :
            t('analytics.download_btn', { plural: profile.plural })
        }
    </Button>
}