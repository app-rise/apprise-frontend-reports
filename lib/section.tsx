import { useT } from 'apprise-frontend-core/intl/language'
import { elementProxyRole } from 'apprise-frontend-core/utils/common'
import { Section } from 'apprise-ui/scaffold/scaffold'
import { PropsWithChildren } from 'react'
import { AnalyticsProfileContext } from './context'
import { AnalyticsDownloadProvider } from './downloadpanel'
import { EditPanel } from './editpanel'
import { EditStateProvider } from './editprovider'
import { AnalyticsProfile, defaultAnalyticsProfile } from './profile'
import { useSingleDetailSection } from './sectiondetail'
import { useMasterDetailSection } from './sectionlist'





// main client-facing api: returns a section based on the application profile.
// falls back to a default profile if and where the application profile is lacking. 

export const useAnalyticsSection = (profile: Partial<AnalyticsProfile> = defaultAnalyticsProfile) => {

    const completeProfile = { ...defaultAnalyticsProfile, ...profile }

    const listSection = useMasterDetailSection(completeProfile)
    const detailSection = useSingleDetailSection(completeProfile)

    return profile.presentation === 'list' ? listSection : detailSection


}

// used internally for any type of section, provides the profile to children.
export const AnalyticsSection = (props: PropsWithChildren<AnalyticsProfile>) => {

    const t = useT()

    const { children, ...profile } = props

    const { SectionIcon, plural, route } = profile

    return <Section icon={<SectionIcon />} route={route} title={t(plural)} >
        <AnalyticsProfileContext.Provider value={profile}>
            <AnalyticsDownloadProvider>
                <EditStateProvider>
                    {children}
                    <EditPanel />
                </EditStateProvider>
            </AnalyticsDownloadProvider>
        </AnalyticsProfileContext.Provider>
    </Section>

}

// marks it a proxy, so the Scaffold picks up the Sectiom inside it.
AnalyticsSection[elementProxyRole] = true



