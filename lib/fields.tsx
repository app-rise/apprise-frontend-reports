import { useT } from 'apprise-frontend-core/intl/language'
import { FormState } from 'apprise-frontend-core/utils/form'
import { useValidation } from 'apprise-ui/field/validation'
import { Dataview, useDataviewModel } from './model'
import { useAnalyticsProfile } from './profile'
import { AnalyticsSettings } from './settings'
import { useAnalyticsStore } from './store'

export const useEditorFields = (props: FormState<Dataview>) => {

    const t = useT()

    const model = useDataviewModel()
    const store = useAnalyticsStore()

    const { singular, plural } =  useAnalyticsProfile()
 
    const { check, is, reportOf } = useValidation()

    const {edited, initial} = props

    const otherViews = store.all().filter(u => u.id !== initial.id)

    const noView = model.isNoDataview(edited)
 
    const self = {

        predefinedView: {

            label: t('analytics.edit_predefined_lbl',{singular}),
            msg: t('analytics.edit_predefined_msg'),
            help: t('analytics.edit_predefined_help',{plural:plural.toLowerCase()})

        }

        ,

        landingDetail: {

            label: t('analytics.edit_landing_lbl',{singular}),
            msg: t('analytics.edit_landing_msg'),
            help: t('analytics.edit_landing_help',{singular:singular.toLowerCase()})

        }
        
        ,


        name: {

            label: t('analytics.edit_name_lbl'),
            msg: t('analytics.edit_name_msg'),
            help: t('analytics.edit_name_help',{plural:plural.toLowerCase()}),
            
            ...check(is.noLanguages()).on(edited.name),

            ...check(is.duplicateLanguages(otherViews.map(t=>t.name))).on(edited.name)

        }
        ,

        description: {

            label: t('analytics.edit_description_lbl'),
            msg: t('analytics.edit_description_msg'),
            help: t('analytics.edit_description_help',{plural:plural.toLowerCase()}),
            
            ...check(is.empty).on(edited.name)

        }

        ,

        views: {

            label: t('analytics.edit_views_lbl'),
            msg: t('analytics.edit_views_msg'),
            help: t('analytics.edit_views_help',{singular: singular.toLowerCase()}),
            
            ...check(is.empty).provided(!noView).on(edited.views)

        }

        ,

        note: {

            label: t('analytics.edit_note_lbl'),
            msg: t('analytics.edit_note_msg'),
            help: t('analytics.edit_note_help',{singular:singular.toLowerCase()})

        }

    }

    return reportOf(self)
}


export const useAnalyticsSettingsFields = (_:AnalyticsSettings) => {

    const t = useT()

    return {

        threshold: {

            label: t('analytics.download_threshold_label'),
            msg: t('analytics.download_threshold_msg'),
            help: t('analytics.download_threshold_help')
        },

        cacheEnabled: {

            label: t('analytics.cache_enabled_label'),
            msg: t('analytics.cache_enabled_msg'),
            help: t('analytics.cache_enabled_help')
        },

        
        resetCache: {

            label: t('analytics.cache_reset_label'),
            msg: t('analytics.cache_reset_msg'),
            help: t('analytics.cache_reset_help')
        }

        ,

        defaultDetail: {

            label: t('analytics.default_detail_label'),
            msg: t('analytics.default_detail_msg'),
            help: t('analytics.default_detail_help')
        }
    }
}