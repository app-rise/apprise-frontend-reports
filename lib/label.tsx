import { useT } from 'apprise-frontend-core/intl/language'
import { useL } from 'apprise-frontend-core/intl/multilang'
import { classname } from 'apprise-ui/component/model'
import { Label, LabelProps, LabelRow, LabelRowProps } from 'apprise-ui/label/label'
import { LockIcon } from 'apprise-ui/utils/icons'
import format from 'date-fns/format'
import formatDistanceToNow from 'date-fns/formatDistanceToNow'
import { cloneElement, useContext } from 'react'
import { AiOutlineClockCircle } from 'react-icons/ai'
import { CardViewIcon, CustomDataViewIcon, DashboardViewIcon, ViewParamIcon } from './constants'
import { LockedParamContext, ViewStoreContext } from './context'
import { Dataview, View } from './model'
import { useAnalyticsProfile } from './profile'
import { useAnalyticsRouting } from './routing'
import { useAnalyticsStore } from './store'
import { ViewParam, useViewParams } from './viewparams'

export type DataviewLabelProps = LabelProps & {


    dataview: string | Dataview
}


export const DataviewLabel = (props: DataviewLabelProps) => {

    const routing = useAnalyticsRouting()
    const store = useAnalyticsStore();

    const { dataview, className, ...rest } = props

    const { DataViewIcon } = useAnalyticsProfile()

    // have nothing, render nothing.
    if (!dataview)
        return null


    const resolved = typeof dataview === 'string' ? store.lookup(dataview)! : dataview // todo: replace with safelookup

    const route = routing.detailRoute(resolved.id)

    const Icon = resolved?.predefined ? DataViewIcon : CustomDataViewIcon

    return <Label className={classname('dataview-label', className)} linkTo={route} icon={<Icon />} title={resolved.name} {...rest} />
}


export type ViewLabelProps = LabelProps & {

    displayMode?: 'name' | 'tab'
    view: View
}


export const ViewLabel = (props: ViewLabelProps) => {

    const l = useL()

    const { view, className, displayMode = 'name', ...rest } = props

    const Icon = view.type === 'dashboard' ? DashboardViewIcon : CardViewIcon

    const name = displayMode === 'name' ? view.name : l(view.tab) ?? view.name

    return <Label className={classname('view-label', className)} icon={<Icon />} title={name} {...rest} />

}


export type ViewParamProps = LabelProps & {

    param: ViewParam
}

export const ViewParamLabel = (props: ViewParamProps) => {

    const { param, className, ...rest } = props

    const params = useViewParams()

    const Icon = param.icon ?? ViewParamIcon

    return <Label className={classname('view-param-label', className)} icon={<Icon />} title={params.nameOf(param)} {...rest} />

}

export type LastRefreshedLabelProps = LabelProps


export const LastRefreshedLabel = (props: LastRefreshedLabelProps) => {

    const t = useT()

    const { stats } = useContext(ViewStoreContext).get()

    if (!stats.lastRefreshed)
        return null


    return <Label mode='tag' fill='lightseagreen' icon={<AiOutlineClockCircle />}
        tip={t("analytics.refresh_timestamp_tip", { date: formatDistanceToNow(new Date(stats.lastRefreshed), { addSuffix: true }) })}
        title={t("analytics.refresh_timestamp", { timestamp: format(new Date(stats.lastRefreshed), 'dd/MM/yyyy HH:mm') })}
        {...props} />

}


export const LockedParamsLabels = (props: Omit<LabelRowProps,'children'>) => {
    
    const t = useT()

    const params = useViewParams()
    const { params: locked } = useContext(LockedParamContext).get()


   return <LabelRow mode='tag' fill='coral' {...props}>{[

       ...params.all().filter(p=>p.lockable).map(p => {
    
            const Icon = p.icon ?? LockIcon

            const lockedValue = locked[p.key ?? p.id]
            const renderedValue = lockedValue ?  p.renderValue?.(lockedValue) ?? lockedValue : t('analytics.locked_all')
            
            const title = `${params.nameOf(p)}: ${renderedValue}`
            
            const tip = lockedValue ? t('analytics.locked_tip',{name:params.nameOf(p)}) : t('analytics.locked_none',{name:params.nameOf(p)?.toLocaleLowerCase()})

           return  <Label key={p.id} icon={<Icon />} tip={tip} title={title} />
    
    
    })

    ]}</LabelRow>
}

export const RepositoryLabels = (props: Omit<LabelRowProps,'children'>) => {


    const { stats } = useContext(ViewStoreContext).get()

    if (!stats.lastRefreshed)
        return null


    return <LabelRow mode='tag' {...props}>{[

        <LastRefreshedLabel key='lastrefreshed' />
        ,

        ...stats.pills.map((e,i) => cloneElement(e,{key:i}))

    ]}</LabelRow>

}