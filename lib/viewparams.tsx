import { useT } from 'apprise-frontend-core/intl/language';
import { State } from 'apprise-frontend-core/state/api';
import { StateProvider } from 'apprise-frontend-core/state/provider';
import { Fielded } from 'apprise-ui/field/model';
import { FC, PropsWithChildren, createContext, useContext } from 'react';
import { IconBaseProps } from 'react-icons';
import { AnalyticsDownloadContext, LockedParamContext } from './context';
import { Dataview, View } from './model';
import { useAnalyticsProfile } from './profile';


export type LockedParams = {


    params: Record<ViewParam['id'], string>

}


export type ViewParam = {

    id: string
    key?: string
    icon?: FC<IconBaseProps>
    default?: boolean
    lockable?: boolean
    lockedInline?: boolean
    lockField?: FC<LockFieldProps>
    range?: () => string[]
    renderValue?: (v: any) => string
    initialValue?: (view?: View) => string | undefined
}

export type LockFieldProps<T = any> = Fielded<T> & { children: T | T[], renderMode: 'lock' | 'download' | 'lock-inline' }



// internal.
export const useViewParams = () => {

    const t = useT()

    const lockedParams = useContext(LockedParamContext)

    const downloadParams = useContext(AnalyticsDownloadContext)

    const { useParameters } =  useAnalyticsProfile()

    const parameters = useParameters()

    const self = {

        all: () => [...(parameters ?? [])].sort(self.comparator)

        ,

        allIn: (view: View) => self.all().filter(p => view.params.includes(p.id))

        ,

        lookup: (id: string) => self.all().find(p => p.id === id || p.key === id)

        ,

        nameOf: (param: ViewParam) => t(`analytics.param_${param.id}`, param.id)

        ,

        comparator: (p1: ViewParam, p2: ViewParam) => self.nameOf(p1).localeCompare(self.nameOf(p2))

        ,

        // if a dataview has no view that doesn't hide with a lockable param, then nothing can be downloaded.
        canDownload: (dataview: Dataview) => {

            const lockables = self.all().filter(p => p.lockable)

            const hasNoUnlockedView = dataview.views.every(v => lockables.some(lp => v.hideOnLocked?.includes(lp.id)))

            return !hasNoUnlockedView
        }

        ,

        // if a dataview has now view
        canGenerate: (dataview: Dataview) => {

            const lockables = Object.keys(lockedParams.get().params)

            const hasNoUnlockedView = dataview.views.every(v => lockables.some(lp => v.hideOnLocked?.includes(lp)))

            return !hasNoUnlockedView
        }


        ,

        lock: (param: ViewParam, value: any) => {

            lockedParams.set(s => {
                if (value)
                    s.params[param.key ?? param.id] = value
                else delete s.params[param.key ?? param.id]
            })

            downloadParams.set(s => {
                if (value)
                    s.params[param.key ?? param.id] = value
                else delete s.params[param.key ?? param.id]
            })
        }

        ,

        lockOf: (param: ViewParam) => lockedParams.get().params[param.key ?? param.id]

    }

    return self
}

export type ViewParamState = {

    params: ViewParams
}

export type ViewParams = Record<ViewParam['id'], any>

export const ViewParamContext = createContext<State<ViewParamState>>(undefined!)

const initialViewParamState: ViewParamState = {

    params: {}

}
export const ViewParamProvider = (props: PropsWithChildren) => {

    const { children } = props

    return <StateProvider context={ViewParamContext} initialState={initialViewParamState}>
        {children}
    </StateProvider>
}