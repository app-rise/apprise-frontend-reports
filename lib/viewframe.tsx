import { useT } from 'apprise-frontend-core/intl/language'
import { useBusyGuardIndicator } from 'apprise-frontend-core/utils/busyguard'
import { useContext, useLayoutEffect, useRef, useState } from 'react'
import { LockedParamContext } from './context'
import { View } from './model'
import framestyles from './viewframe.scss?inline'
import { ViewParam, ViewParamContext, useViewParams } from './viewparams'

// fetches the view 


export type ViewFrameProps = {

    //id: string
    view: View

}



export const displayModeParam: ViewParam = {

    id: 'display_mode',
    initialValue: () => 'Show Totals'


}


export const ViewFrame = (props: ViewFrameProps) => {

    const t = useT()

    const { view } = props

    // true until iframe load event, drives the spinner.
    const [wait, setWait] = useState<boolean>(true)

    const Indicator = useBusyGuardIndicator()

    // tracked param values, shareable cross-tabs.
    const params = useContext(ViewParamContext)
    const lockedParams = useContext(LockedParamContext)

    const containerRef = useRef<HTMLDivElement>(null)

    const viewparams = useViewParams().allIn(view)

    // mounts cusotm frame styles on mount (includes HMR remounts)
    useLayoutEffect(() => {

        const iframe = containerRef.current?.getElementsByClassName(`viewframe`)[0] as HTMLIFrameElement

        const framewindow = iframe.contentWindow as Window

        var mountstyles = () => {

            const styleid = 'viewframestyles'

            var framebody = iframe.contentDocument?.body as HTMLBodyElement

            var currentstyles = iframe.contentDocument!.getElementById(styleid)

            if (currentstyles)
                framebody.removeChild(currentstyles)

            var newstyles = document.createElement("style")

            framebody?.appendChild(newstyles)

            newstyles.id = styleid

            newstyles.innerHTML += framestyles


        }

        // we apply styles also when component is remounted in HMR.
        mountstyles()


        var onframeload = () => {

            mountstyles()

            const framecontents = iframe.contentDocument?.querySelector('#root [data-testid="embed-frame"] > div:first-of-type') as HTMLElement

            const config: MutationObserverInit = { attributes: false, characterData: true, childList: true, subtree: true };

            // listen for spinner removal to remove our own.
            new MutationObserver((mutationList, observer) => {

                for (const mutation of mutationList) {

                    for (const removedNode of mutation.removedNodes)
                        if (removedNode['querySelector'])
                            if ((removedNode as HTMLElement).querySelector('[data-testid="loading-indicator"]')) {
                                setWait(false)
                                observer.disconnect()
                            }
                            // else {
                            //     console.log(removedNode)     // decomment to capture loading indicatgor if it changes.
                            // }
                }

            }).observe(iframe.contentDocument!, config);


            // listens for filters to patch them.
            [...viewparams, displayModeParam].forEach(p => new MutationObserver((mutationList, thisObserver) => {

                const name = (p.key ?? p.id).replaceAll("_", " ")

                const onDisplayModeChange = (value: string | null) => {

                    switch (value) {
                        case "Show Totals": framecontents.className = `${framecontents.className} nosort`; break;
                        default: framecontents.className = framecontents.className.replaceAll('nosort', '')
                    }

                }

                for (const mutation of mutationList) {
                    for (const added of mutation.addedNodes) {

                        const div = (added as HTMLElement)?.querySelector?.(`[aria-label="${name}" i]`) as HTMLDivElement

                         if (div) {

                            // initial setting
                            if (p === displayModeParam)
                                onDisplayModeChange("Show Totals")


                            const onChange = (value: string | null) => {

                                if (p === displayModeParam)
                                    onDisplayModeChange(value)

                                else {
                                    params.set(s => s.params[p.key ?? p.id] = value)
                                }
                            }

                            patchParam(view, p, div, onChange, lockedParams.get().params)
                            thisObserver.disconnect()
                        }

                        const td= (added as HTMLElement)?.querySelector?.(`td`)

                        if (td){

                            const span = (td.querySelector('span') as HTMLSpanElement)

                            if (span.textContent?.toLocaleLowerCase() === 'totals')
                                td.className = `${td.className} totals-cell`
                            }
                    }



                }

            }).observe(iframe.contentDocument!, config))


            // const onResize = (entries: ResizeObserverEntry[]) => {

            //     if (containerRef.current)

            //         for (const entry of entries) {

            //             const footerheight = 60
            //             containerRef.current.style.height = `${entry.contentRect.bottom + footerheight}px`

            //             // console.log("resized @", entry.contentRect.height)
            //         }

            // }


           // const resizeObserver = new ResizeObserver(debounce(onResize, 100))

            // if (view.type === 'dashboard')
            //     resizeObserver.observe(framecontents)

            framewindow.removeEventListener('DOMContentLoaded', onframeload)

        }


        framewindow.addEventListener('DOMContentLoaded', onframeload)

        // eslint-disable-next-line
    }, [])


    return <Indicator wait={wait} msg={t('analytics.generate_msg')}>
        <div ref={containerRef} className='report-view'>
            <iframe title="viewframe" className='viewframe' src={`/${view.embedUrl}#titled=false&bordered=false`} />
        </div>
    </Indicator>

}

function patchParam(view: View, p: ViewParam, div: HTMLDivElement, onChange: (_: string | null) => void, lockedParams: Record<string, string>) {

    // until MB supports required dashboard params (planned!), we hide & disable the remove icon on the dropdown.
    if (p.initialValue) {

        // remove pointer event so can't trigger remove.
        div.style.pointerEvents = 'none'

        // change svg vkisuals to caret.
        div.querySelector('svg.Icon-close path')?.setAttribute('d', `M 1.47 4.47 a 0.75 0.75 0 0 1 1.06 0 L 8 9.94 l 5.47 -5.47 a 0.75 0.75 0 1 1 1.06 1.06 l -6 6 a 0.75 0.75 0 0 1 -1.06 0 l -6 -6 a 0.75 0.75 0 0 1 0 -1.06 Z`)
    }

    //console.log({lockedParams, param:p})

    if (lockedParams[p.key ?? p.id])
        div.className = `${div.className} param-locked`

    else

        // listens for filters to patch them.
        new MutationObserver(() => {

            const span = div.querySelector?.('span') as HTMLSpanElement

            onChange(span?.textContent)

        }).observe(div, { characterDataOldValue: true, childList: true, characterData: true, subtree: true })


}