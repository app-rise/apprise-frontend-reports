import { useT } from 'apprise-frontend-core/intl/language';
import { useEventBus } from 'apprise-frontend-events/bus';
import { SettingRenderProps, SettingsModule } from 'apprise-frontend-streams/settings/model';
import { Button } from 'apprise-ui/button/button';
import { Field } from 'apprise-ui/field/field';
import { Form } from 'apprise-ui/form/form';
import { SwitchBox } from 'apprise-ui/switchbox/switchbox';
import { useFeedback } from 'apprise-ui/utils/feedback';
import { DataViewIcon, cache_refresh_topic, analyticsType } from './constants';
import { useAnalyticsSettingsFields } from './fields';
import { NumberBox } from 'apprise-ui/numberbox/numberbox';


export type AnalyticsSettings = {

    downloadThreshold: number
    cacheEnabled: boolean

}

const defaultDownoadThreshold = 100

export const AnalyticsSettingsPanel = (props: SettingRenderProps<AnalyticsSettings>) => {

    const t = useT()
    const events = useEventBus()
    const feedback = useFeedback()

    const { onChange, settings } = props

    const fields = useAnalyticsSettingsFields(settings)

    const resetCache = () => {
        events.publish(cache_refresh_topic, {})
        feedback.showNotification(t('analytics.cache_resetted'))
    }

    return <Form>

        <NumberBox info={fields.threshold} min={1} width={75} onChange={threshold => onChange({ ...settings, downloadThreshold: threshold ?? defaultDownoadThreshold})}>
            {settings.downloadThreshold}
        </NumberBox>

        <SwitchBox info={fields.cacheEnabled} onChange={enabled => onChange(enabled ? { ...settings, cacheEnabled: true } : { ...settings, cacheEnabled: false })} >
            {settings.cacheEnabled}
        </SwitchBox>

        <Field info={fields.resetCache}>
            <Button enabled={settings.cacheEnabled} onClick={resetCache} type='primary'>{t('analytics.cache_reset_btn')}</Button>
        </Field>

    </Form>
}

export const useAnalyticsSettingsModule = () : SettingsModule<AnalyticsSettings> => {

    return {

        Icon: DataViewIcon,
        name: 'analytics.settings_name',
        defaults: {
            downloadThreshold: defaultDownoadThreshold
        },
        type: analyticsType,
        Render: AnalyticsSettingsPanel,
        fields: useAnalyticsSettingsFields

    }
}

