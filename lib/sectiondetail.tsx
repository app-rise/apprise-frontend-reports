import { useT } from 'apprise-frontend-core/intl/language'
import { elementProxyRole } from 'apprise-frontend-core/utils/common'
import { AnalyticsProfile } from './profile'


import { useStable } from 'apprise-frontend-core/utils/function'
import { Crumb } from 'apprise-ui/scaffold/scaffold'

import { NoSuchRoute } from 'apprise-ui/link/nosuchroute'
import { Redirect, Route, Switch } from 'react-router-dom'
import { DataviewDetail } from './detail'
import { EmptyDetail } from './detailempty'
import { useAnalyticsRouting } from './routing'
import { AnalyticsSection } from './section'
import { useAnalyticsStore } from './store'

// returns a section with a single-detail UI.

export const useSingleDetailSection = (profile: AnalyticsProfile) => {

    const { route, plural } = profile

    const t = useT()

    const Section = useStable(() => <AnalyticsSection {...profile} >
        <SingleDatailSection />
    </AnalyticsSection>)

    Section[elementProxyRole] = true

    const crumbs = <>
        <Crumb path={route}>{t(plural)}</Crumb>
    </>

    return { Section, crumbs }
}


const SingleDatailSection = () => {

    const routing = useAnalyticsRouting()

    const store = useAnalyticsStore()

    const views = store.all()

    const landingDetail = views.find(v => v.landingDetail) ?? views[0]

    return <Switch>


        {views.length ? 
        
            landingDetail &&
        
            <Route exact path={routing.listRoute()}>

                <Redirect to={routing.detailRoute(landingDetail.id)} />
            </Route>

            :

            <Route exact path={routing.listRoute()}>
                <EmptyDetail />
            </Route>
        }

        <Route exact path={routing.detailRoute(':id')}>
            <DataviewDetail />
        </Route>

        <NoSuchRoute />

    </Switch>


}