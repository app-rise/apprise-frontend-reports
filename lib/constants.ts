import { BiTargetLock } from 'react-icons/bi'
import { BsViewList } from 'react-icons/bs'
import { MdOutlineSpaceDashboard, MdSpaceDashboard } from 'react-icons/md'


export const analyticsType = "analytics"

export const DataviewSectionIcon = MdOutlineSpaceDashboard
export const DataViewIcon =  MdSpaceDashboard
export const CustomDataViewIcon = MdOutlineSpaceDashboard

export const dataviewSingular =  'analytics.singular'
export const dataviewPlural = 'analytics.plural'

export const analyticsRoute = "/analytics"

export const viewSingular =  'analytics.viewSingular'
export const viewPlural = 'analytics.viewPlural'

export const DashboardViewIcon = MdSpaceDashboard
export const CardViewIcon = BsViewList
export const ViewParamIcon = BiTargetLock

export const cache_refresh_topic = 'cache_refresh'

export const sortParam = 'sorting'




