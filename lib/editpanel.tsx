import { useMode } from 'apprise-frontend-core/config/api'
import { useT } from 'apprise-frontend-core/intl/language'
import { NoteBox } from 'apprise-frontend-iam/user/notebox'
import { Button, ButtonRow } from 'apprise-ui/button/button'
import { Form } from 'apprise-ui/form/form'
import { GroupBox } from 'apprise-ui/groupbox/groupbox'
import { RouteGuard } from 'apprise-ui/link/routeguard'
import { MultiBox } from 'apprise-ui/multibox/multibox'
import { MultiSelectBox, SelectBox } from 'apprise-ui/selectbox/selectbox'
import { SwitchBox } from 'apprise-ui/switchbox/switchbox'
import { Tip } from 'apprise-ui/tooltip/tip'
import { Topbar } from 'apprise-ui/topbar/topbar'
import { EditIcon, RevertIcon, SaveIcon } from 'apprise-ui/utils/icons'
import { useDeferredRouting } from 'apprise-ui/utils/routing'
import { Fragment, useContext } from 'react'
import { AiFillMinusCircle } from 'react-icons/ai'
import { MdOutlineDragIndicator } from 'react-icons/md'
import { useAnalyticsClient } from './client'
import { AnalyticsEditContext } from './context'
import "./editpanel.scss"
import { ViewLabel, ViewParamLabel } from './label'
import { View, useViewModel } from './model'
import { useAnalyticsProfile } from './profile'
import { useAnalyticsStore } from './store'
import { useViewParams } from './viewparams'

export const EditPanel = () => {

    const t = useT()

    const { editkit } = useContext(AnalyticsEditContext)

    const { singular } = useAnalyticsProfile()

    const { Drawer } = editkit

    return <Drawer icon={<EditIcon />} width={800} title={t('analytics.edit_title', { singular })}>
        <Inner />
    </Drawer>
}

export const Inner = () => {

    const t = useT()
    const mode = useMode()

    const { presentation, noPredefined } = useAnalyticsProfile()

    const { routeAfterRenderTo } = useDeferredRouting()

    const { edited, dirty, errors, editkit, reset, set, fields } = useContext(AnalyticsEditContext)

    const model = useViewModel();
    const store = useAnalyticsStore()
    const client = useAnalyticsClient()
    const params = useViewParams()

    const save = () => client.save(edited).then(saved => reset.to(saved).quietly()).then(() => routeAfterRenderTo(editkit.closedRoute))
    const revert = reset.toInitial.confirm
    const remove = () => client.remove(edited)

    const allViews = store.allViewsSorted()
    const allParams = params.all()

    const viewIds = edited.views.map(v => v.id)

    const defaultParamIds = allParams.filter(p => p.default).map(p => p.id)

    const availableViews = allViews.filter(v => !viewIds.includes(v.id))

    const initView = (v: View, old?: View | undefined) => ({ ...v, params: old?.params ?? defaultParamIds })

    const landingIsDefined = store.all().some(v => v.landingDetail && v.id !== edited.id)

    return <Fragment>
        <Topbar>

            <ButtonRow>

                <Button type='danger' iconPlacement='left' icon={<AiFillMinusCircle />} disabled={mode.production && edited.predefined} onClick={remove}>
                    {t("analytics.remove_btn")}
                </Button>

                <Button type='primary' icon={<SaveIcon />} dot={errors > 0} enabled={dirty} disabled={errors > 0} onClick={save}>
                    {t("analytics.edit_save_btn")}
                </Button>

                <Button icon={<RevertIcon />} dot={errors > 0} enabled={dirty} disabled={errors > 0} onClick={revert}>
                    {t("analytics.edit_revert_btn")}
                </Button>

            </ButtonRow>

        </Topbar>


        <Form className='edit-panel'>

            {noPredefined ||
                <SwitchBox info={fields.predefinedView} onChange={set.with((m, t) => m.predefined = t)}>{edited.predefined}</SwitchBox>
            }

            {presentation === 'detail' &&
                <SwitchBox info={fields.landingDetail} disabled={landingIsDefined} onChange={set.with((m, t) => m.landingDetail = t)}>{edited.landingDetail}</SwitchBox>
            }


            <MultiBox info={fields.name} onChange={set.with((m, t) => m.name = t)}>
                {edited.name}
            </MultiBox>

            <MultiBox info={fields.description} multi={4} onChange={set.with((m, t) => m.description = t)}>
                {edited.description}
            </MultiBox>

            <GroupBox info={fields.views} name={t('analytics.viewSingular')}
                onChange={set.with((m, t) => m.views = t)} addIf={availableViews.length > 0} newItem={() => initView(availableViews[0])}
                render={(view, index) =>

                    <div className='view-item'>

                        <div className='item-handle' >
                            <Tip tip={t('analytics.edit_views_drag_tip')}>
                                <MdOutlineDragIndicator />
                            </Tip>
                        </div>

                        <div className='view-item-contents'>

                            <SelectBox horizontal label={t('analytics.edit_views_name_lbl')} noClear
                                keyOf={v => `${v.type}-${v.id}`}
                                textOf={model.textOf}
                                options={allViews}
                                render={v => <ViewLabel bare view={v} />}
                                onChange={set.with((m, v) => m.views[index] = initView(v, m.views[index]))}>
                                {view}
                            </SelectBox>

                            <MultiSelectBox horizontal label={t('analytics.edit_views_params_lbl')}
                                keyOf={v => v.id}
                                textOf={params.nameOf}
                                options={allParams}
                                render={p => <ViewParamLabel param={p} />}
                                onChange={set.with((m, ps) => {
                                    m.views[index].params = ps.map(p => p.id)
                                })}>
                                {allParams.filter(p => view.params?.includes(p.id))}
                            </MultiSelectBox>

                            <MultiSelectBox style={{ marginTop: 2 }} horizontal label={t('analytics.edit_views_hidelocked_params_lbl')}
                                keyOf={v => v.id}
                                textOf={params.nameOf}
                                options={allParams}
                                render={p => <ViewParamLabel param={p} />}
                                onChange={set.with((m, ps) => m.views[index].hideOnLocked = ps.map(p => p.id))}>
                                {allParams.filter(p => view.hideOnLocked?.includes(p.id))}
                            </MultiSelectBox>

                            <MultiBox placeholder={{ en: view.name }} horizontal label={t('analytics.edit_views_tab_lbl')} onChange={set.with((m, t) => m.views[index].tab = t)}>
                                {view.tab}
                            </MultiBox>

                        </div>
                    </div>

                }>
                {edited.views}
            </GroupBox>

            <NoteBox info={fields.note} onChange={set.with((m, t) => m.note = t)}>
                {edited.note}
            </NoteBox>


            <RouteGuard when={dirty} ignoreQueryParams={false} />

        </Form>
    </Fragment>
}