import { dataviewapi, dataviewservice, viewapi } from './calls'





export const useReportPreloaders = () => {

    return [

        { name: dataviewapi, task: call => call.at(dataviewapi, dataviewservice).get()},
        { name: viewapi, task: call => call.at(viewapi, dataviewservice).get() }

    ]
}

