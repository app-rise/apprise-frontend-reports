import { useT } from 'apprise-frontend-core/intl/language'
import { Form } from 'apprise-ui/form/form'
import { Explainer, ExplainerPanel } from 'apprise-ui/utils/explainer'
import { LockIcon } from 'apprise-ui/utils/icons'
import { useContext } from 'react'
import { BiTargetLock } from 'react-icons/bi'
import { AnalyticsEditContext } from './context'

import { useAnalyticsProfile } from './profile'
import { useViewParams } from './viewparams'

export const LockPanel = () => {

    const t = useT()

    const { lockkit } = useContext(AnalyticsEditContext)

    const { singular } =  useAnalyticsProfile()

    const { Drawer } = lockkit

    return <Drawer width={500} icon={<LockIcon />} title={t('analytics.lock_title', { singular })}>
        <Inner />
    </Drawer>
}

export const Inner = () => {

    const t = useT()

    const { singular, plural } =  useAnalyticsProfile()
    
    const params = useViewParams()


    const lockableParams = params.all().filter(p => p.lockable)

    return <ExplainerPanel>

        <Explainer icon={<BiTargetLock />} iconFill>
            {t('analytics.lock_explainer', { singular: singular.toLowerCase(), plural: plural.toLowerCase() })}
        </Explainer>

        <Form>
            {lockableParams.map(param => {

                const Field = param.lockField!

                return <Field renderMode='lock' label={params.nameOf(param)} className='locked-param' key={param.id} onChange={p => params.lock(param, p)}>
                    {params.lockOf(param)}
                </Field>
            })}
        </Form>


    </ExplainerPanel>
}