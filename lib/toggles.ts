import { useToggles } from 'apprise-frontend-core/toggle/toggles'



export const allToggles = ["dashboards"] as const

export type AnalyticsToggles = typeof allToggles[number]

export const useAnalyticsToggles = () => {

    const toggles =  useToggles<AnalyticsToggles>() 
    const self = {

        get dashboards() { 
            return toggles.isActive('dashboards')
        }
    }


    return self

}