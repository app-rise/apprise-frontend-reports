import { useT } from 'apprise-frontend-core/intl/language'
import { useL } from 'apprise-frontend-core/intl/multilang'
import { Button } from 'apprise-ui/button/button'
import { classname } from 'apprise-ui/component/model'
import { Tip } from 'apprise-ui/tooltip/tip'
import { DownloadIcon, EditIcon } from 'apprise-ui/utils/icons'
import { useContext } from 'react'
import { IoMdPlay } from 'react-icons/io'
import './card.scss'
import { AnalyticsDownloadContext, AnalyticsEditContext, AnalyticsDownloadStatsContext } from './context'
import { useDownloadDrawer } from './downloadpanel'
import { DataviewLabel } from './label'
import { Dataview, useDataviewModel } from './model'
import { useAnalyticsOracle } from './oracle'
import { AnayticsPreferencs } from './preferences'
import { useAnalyticsRouting } from './routing'
import { useViewParams } from './viewparams'


export const DataviewCard = (props: {

    dataview: Dataview
    displayMode?: AnayticsPreferencs['analytics']['displayMode']

}) => {

    const t = useT()
    const l = useL()


    const model = useDataviewModel()
    const routing = useAnalyticsRouting()
    const oracle = useAnalyticsOracle()
    const params = useViewParams()

    const { editkit, edited } = useContext(AnalyticsEditContext)

    const downloadkit = useDownloadDrawer()

    const downloadCtx = useContext(AnalyticsDownloadContext)
    const downloadStatsCtx = useContext(AnalyticsDownloadStatsContext)

    const { dataview, displayMode = 'compact' } = props

    const generate = () => routing.routeToDetail(dataview.id)
    

    const download = () => {

        downloadCtx.set(s => {
            s.dataviews = [dataview]
        })
        downloadkit.toggle()
    }

    const classes = classname('dataview-card', `card-mode-${displayMode}`, dataview.predefined && 'dataview-predefined', model.isNoDataview(edited) || (edited.id === dataview.id ? 'dataview-selected' : 'dataview-unselected'))

    const tip = displayMode === 'compact' && dataview.description ? dataview.description : null

    const downloading = downloadStatsCtx.get().stats?.status === 'inprogress' && !!downloadStatsCtx.get().stats?.todo

    const downloadDisabled = downloading || !params.canDownload(dataview)

    return <div className={classes}>

        <div className='card-contents'>

            <DataviewLabel tip={tip} linkTo={routing.detailRoute(dataview.id)} noLink={displayMode === 'full'} tipDelay={1} dataview={dataview} />

            <div className='card-description'>
                <Tip tipDelay={1.5} tip={dataview.description}>
                    {l(dataview.description)}
                </Tip>
            </div>

            {
                displayMode === 'full' &&

                <div className='card-btns'>

                    <Button iconPlacement='left' disabled={!params.canGenerate(dataview)} icon={<IoMdPlay />} onClick={generate}>
                        {t('analytics.generate_btn')}
                    </Button>

                    <Button disabled={downloadDisabled} iconPlacement='left' icon={<DownloadIcon />} onClick={download}>
                        {t('analytics.download_one_btn')}
                    </Button>
                    


                </div>
            }


        </div>
        <div className='card-bar'>

            {oracle.canEdit(dataview) &&

                <div className='card-btn'  >
                    <EditIcon onClick={() => editkit.openAt(dataview.id)} />
                </div>
            }

            {displayMode === 'compact' &&

                <div className={classname('card-btn', downloadDisabled && 'card-btn-disabled')}  >
                    <DownloadIcon onClick={download} />
                </div>
            }

        </div>

    </div>

}