import { useT } from 'apprise-frontend-core/intl/language'
import { useL } from 'apprise-frontend-core/intl/multilang'
import { classname } from 'apprise-ui/component/model'
import { Label, LabelProps, LabelRow } from 'apprise-ui/label/label'
import { LifecycleSummary } from 'apprise-ui/lifeycle/lifecyclesummary'
import { NoSuchRoute } from 'apprise-ui/link/nosuchroute'
import { Page } from 'apprise-ui/page/page'
import { SidebarContent } from 'apprise-ui/page/sidebar'
import { SelectBox } from 'apprise-ui/selectbox/selectbox'
import { Table } from 'apprise-ui/table/table'
import { useTableUtils } from 'apprise-ui/table/utils'
import { Tab, useTabs } from 'apprise-ui/tabs/tab'
import { Titlebar } from 'apprise-ui/titlebar/titlebar'
import { Topbar } from 'apprise-ui/topbar/topbar'
import { Fragment, useContext, useMemo } from 'react'
import { useParams } from 'react-router-dom'
import { DataViewIcon } from './constants'
import { AnalyticsEditContext, LockedParamContext } from './context'
import "./detail.scss"
import { DataviewLabel, LockedParamsLabels, RepositoryLabels, ViewLabel } from './label'
import { ViewLoader } from './loader'
import { Dataview, View, useDataviewModel } from './model'
import { useAnalyticsProfile } from './profile'
import { useAnalyticsRouting, viewParam } from './routing'
import { useAnalyticsStore } from './store'
import { ViewFrame } from './viewframe'
import { ViewParamProvider } from './viewparams'

export const DataviewDetail = () => {

    const { id } = useParams<{ id: string }>()

    const store = useAnalyticsStore()

    const dataview = store.lookup(id)

    return dataview ? <Inner key={dataview.id} dataview={dataview} /> : <NoSuchRoute />

}

type InnerProps = {

    dataview: Dataview
}

const Inner = (props: InnerProps) => {

    const t = useT()
    const l = useL()

    const { dataview } = props

    const { presentation } = useAnalyticsProfile()

    const routing = useAnalyticsRouting()
    const store = useAnalyticsStore()

    const edit = useContext(AnalyticsEditContext)

    const pillprops: LabelProps = { bare: true, mode: 'tag' }

    const { params: lockedParams } = useContext(LockedParamContext).get()

    const visibleviews = dataview.views.filter(v => !v.hideOnLocked || v.hideOnLocked.some(p => !lockedParams[p]))

    const multipleviews = visibleviews.length > 1

   
    // note: we don't pass content to "tabs" because we mount them all at the same time.
    // this is for performance, so we don't reload frames at each switch, and all loading is parallelised.
    const { tabs, selectedTab } = useTabs(visibleviews.map((view, i) =>

        <Tab icon={<Fragment />} id={`${i}`} label={<ViewLabel displayMode='tab' view={view} />} />

    ), { param: viewParam })


    const activeView = visibleviews[selectedTab]

    const editBtn = edit.editBtn(dataview)
    const addBtn = edit.addBtn

    const sidelist = useSideList(props)

    const options = store.allSorted().map(v => v.id)

    const masterdetail = presentation === 'list'
    const singledetail = !masterdetail

    const viewSelector = <SelectBox className='view-selector no-tabs' minWidth={300} noClear
        options={options}
        render={v => <DataviewLabel dataview={v} />}
        onChange={v => routing.routeToDetail(v!)}
        {...props} >

        {dataview.id}
    </SelectBox>

    return <Page className='analytics-views'>

        {multipleviews &&

            <Topbar tabs={tabs} />

        }

        <Titlebar title={l(dataview.name)} secondary={singledetail && l(dataview.description)}>
            <LabelRow>
                {dataview.predefined && <Label fill='dodgerblue' icon={<DataViewIcon />} title={t('analytics.predefined')} {...pillprops} />}
                <RepositoryLabels />
                <LockedParamsLabels />
            </LabelRow>
            {masterdetail || viewSelector}
        </Titlebar>

        <SidebarContent>

            {editBtn && <Fragment>
                {editBtn}
                <br />
            </Fragment>}

            {singledetail && addBtn && <Fragment>
                {addBtn}
                <br />
            </Fragment>}

            <LifecycleSummary lifecycle={dataview.lifecycle} />

            <br />

            {masterdetail && sidelist}


        </SidebarContent>

        {/* mounts all tabs at once and tracks the active one to hide all others. */}
        <ViewParamProvider>

            {visibleviews.map((view, i) => {

                const active = view === activeView

                return <div key={view.id} className={classname('view-contents', active || 'view-hidden')}>
                    <ViewLoader active={active} view={view}>{loaded =>
                        <ViewFrame view={loaded} />
                    }</ViewLoader>
                </div>


            })}

        </ViewParamProvider>

    </Page>
}


export type DataViewProps = {

    dataview: Dataview
    view: View
}


const useSideList = (props: InnerProps) => {

    const store = useAnalyticsStore()
    const routing = useAnalyticsRouting()
    const model = useDataviewModel()
    //const oracle = useSubmissionOracle()

    const allDataviews = store.all()
    const { dataview } = props

    // eslint-disable-next-line
    const sortedDataviews = useMemo(() => store.allSorted(), [allDataviews])

    const { Column } = useTableUtils<Dataview>()

    return <SidebarContent.Affix>

        <Table.Sider key={dataview.id} scrollToRow={r => r.id === dataview.id} name={`dataviews`} data={sortedDataviews}>

            <Column text={model.textOf} render={r => <DataviewLabel linkTo={routing.detailRoute(r.id)} highlighted={r.id === dataview.id} dataview={r} />} />

        </Table.Sider>

    </SidebarContent.Affix>
}
