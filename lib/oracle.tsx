import { useActions } from 'apprise-frontend-core/authz/action'
import { useLoggedOracle } from 'apprise-frontend-core/authz/oracle'
import { useTenancyOracle } from 'apprise-frontend-iam/authz/tenant'
import { reportActions } from './actions'
import { Dataview } from './model'


export const useAnalyticsOracle = () => {

    const actions = useActions()

    const logged = {...useTenancyOracle(), ...useLoggedOracle()}

    const self = {

        canManageAll: () => logged.can(reportActions.manage),
        canManage: (r:Dataview) => logged.can(actions.specialise(reportActions.manage,r.id)),
        canEdit: (r:Dataview) => self.canManage(r),
        canRemove: (r:Dataview) => self.canManage(r)

    }

    return self

}