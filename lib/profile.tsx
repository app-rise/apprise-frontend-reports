import { useT } from 'apprise-frontend-core/intl/language'
import { useContext } from 'react'
import { IconType } from 'react-icons'
import { DataViewIcon, DataviewSectionIcon, analyticsRoute, dataviewPlural, dataviewSingular } from './constants'
import { AnalyticsProfileContext } from './context'
import { ViewParam } from './viewparams'


// collects applicaton directives for the user interface.

export type AnalyticsProfile = {

    // master/detail list or single-detail.
    presentation: 'list' | 'detail'

    group: string | undefined,

    //  the "terminology" for data views.
    singular: string
    plural: string

    // the mountpoint.
    route: string

    DataViewIcon: IconType
    SectionIcon: IconType

    noPredefined: boolean,
    noLockedParams: boolean,
    noDownload: boolean,

    useParameters: () => ViewParam[]

}



export const defaultAnalyticsProfile: AnalyticsProfile = {

    presentation: 'list',

    group: undefined,

    route: analyticsRoute,
    plural: dataviewPlural,
    singular: dataviewSingular,

    noPredefined: false,
    noLockedParams: false,
    noDownload: false,

    DataViewIcon,
    SectionIcon: DataviewSectionIcon,

    useParameters: () => []
}

export const useAnalyticsProfile = () => {

    const t = useT()

    const profile = useContext(AnalyticsProfileContext)

    return {...profile, singular: t(profile.singular), plural: t(profile.plural) }
}

