import { Empty } from 'antd'
import { useT } from 'apprise-frontend-core/intl/language'
import { Page } from 'apprise-ui/page/page'
import { CSSProperties, cloneElement, useContext } from 'react'
import { AnalyticsEditContext } from './context'
import { useAnalyticsProfile } from './profile'

export const EmptyDetail = () => {

    const t = useT()

    const { addBtn } = useContext(AnalyticsEditContext)

    const { plural } = useAnalyticsProfile()

    const centered: CSSProperties = { height: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }

    return <Page>

        <div style={centered}>
            <Empty description={t('analytics.no_detail_msg', { plural })} image={Empty.PRESENTED_IMAGE_SIMPLE} />
            {addBtn && cloneElement(addBtn, {type:'primary'} )}
        </div>

    </Page>
}