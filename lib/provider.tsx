
import { StateProvider } from 'apprise-frontend-core/state/provider'
import { AnalyticsProps } from './analytics'
import { DataviewStoreContext, LockedParamContext, ViewStoreContext } from './context'
import { DataviewStore, ViewStore } from './store'
import { LockedParams } from './viewparams'




const initialViewState: ViewStore = {

    views: [],
    stats: {

        lastRefreshed: undefined!,
        pills: []
    }
}


const initialStore: DataviewStore = {

    all: []
}

const initialLockedParams: LockedParams = {

    params: {}
}



export const AnalyticsProvider = (props: AnalyticsProps) => {

    const { children } = props

    return <StateProvider initialState={initialViewState} context={ViewStoreContext}>
        <StateProvider initialState={initialStore} context={DataviewStoreContext}>
            <StateProvider initialState={initialLockedParams} context={LockedParamContext}>
                {children}
            </StateProvider>
        </StateProvider>
    </StateProvider >
}