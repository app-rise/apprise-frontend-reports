import { useT } from 'apprise-frontend-core/intl/language'
import { elementProxyRole } from 'apprise-frontend-core/utils/common'
import { AnalyticsProfile } from './profile'


import { useL } from 'apprise-frontend-core/intl/multilang'
import { useStable } from 'apprise-frontend-core/utils/function'

import { NoSuchRoute } from 'apprise-ui/link/nosuchroute'
import { Crumb } from 'apprise-ui/scaffold/scaffold'
import { Route, Switch } from 'react-router'
import { DataviewDetail } from './detail'
import { DataviewList } from './list'
import { useAnalyticsRouting } from './routing'
import { AnalyticsSection } from './section'
import { useAnalyticsStore } from './store'

// returns a section with a master/detail list UI.
export const useMasterDetailSection = (profile: AnalyticsProfile) => {

    const { route, plural } = profile

    const t = useT()
    const l = useL()

    const store = useAnalyticsStore()

    const Section = useStable(

        () => <AnalyticsSection {...profile} >
            <MasterDetailSection />
        </AnalyticsSection>
    )

    Section[elementProxyRole] = true

    const crumbs = <>
        <Crumb path={route}>{t(plural)}</Crumb>
        <Crumb path={`${route}/*`}>{(path: string[]) => l(store.lookup(path[2])?.name) ?? '?'}</Crumb>
    </>

    return { Section, crumbs }
}


export const MasterDetailSection = () => {


    const routing = useAnalyticsRouting()


    return <Switch>

        <Route exact path={routing.listRoute()}>
            <DataviewList />
        </Route>

        <Route exact path={routing.detailRoute(':id')}>
            <DataviewDetail />
        </Route>

        <NoSuchRoute />


    </Switch>


}