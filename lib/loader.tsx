import { utils } from 'apprise-frontend-core/utils/common'
import { usePrevious } from 'apprise-frontend-core/utils/hooks'
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import { PropsWithChildren, useContext, useEffect, useState } from 'react'
import { AnalyticsProps } from './analytics'
import { useAnalyticsClient, useViewClient } from './client'
import { ViewStoreContext } from './context'
import { View } from './model'
import { ViewParamContext, useViewParams } from './viewparams'



export const AnalyticsLoader = (props: AnalyticsProps) => {

    const { children } = props

    const client = useAnalyticsClient()

    const activate = async () => {

        await Promise.all([client.fetchViews(), client.fetchDataviews()])

    }

    const { content } = useRenderGuard({

        render: children,
        orRun: activate
    })

    return content
}

export const RepositoryStatsLoader = (props: AnalyticsProps) => {

    const views = useContext(ViewStoreContext)

    const loadStats = props.useStatsLoader?.()

    useEffect(()=> {

        loadStats?.().then(stats => views.set(v => v.stats = stats))
        
    // eslint-disable-next-line
    } ,[])

    return null
}

export type ViewLoaderProps = {

    active: boolean
    view: View
    children: (_: View) => PropsWithChildren['children']

}

export const ViewLoader = (props: ViewLoaderProps) => {

    const { view, active, children } = props

    const client = useViewClient()

    const viewparams = useViewParams().allIn(view)

    const params = useContext(ViewParamContext).get().params

    const [fetched, setFetched] = useState<View | undefined>(undefined!)

    const fingerprint = `${viewparams.map(p => params[p.key ?? p.id]).sort().filter(p => !!p).join()}`

    const previousFingerprint = usePrevious(fingerprint)

    // we reload the view/frame if params change when the tab is inactive.
    // (if it's active, the change are handled directly by the frame, no reloading required or welcome)
    useEffect(() => {

        if ((!active && !utils().deepequals(fingerprint, previousFingerprint) ) )
            setFetched(undefined)

    // eslint-disable-next-line
    },[active, fingerprint])

    const { content } = useRenderGuard({
 
        when: !!fetched,

        render: children(fetched!),

        orRun: () => client.fetchView(view,params).then(urls => setFetched({ ...view, ...urls }))
    })

    return content
}
