import { PropsWithChildren } from 'react'
import { AnalyticsLoader, RepositoryStatsLoader } from './loader'
import { AnalyticsProvider } from './provider'
import { AnalyticsRepositoryStats } from './store'


export type AnalyticsProps = PropsWithChildren<{

    useStatsLoader?: () =>  () => Promise<AnalyticsRepositoryStats>

}>

export const Analytics = (props: AnalyticsProps) => {

    const { children } = props

    return <AnalyticsProvider >
            <AnalyticsLoader {...props}>
                <RepositoryStatsLoader {...props} />
                {children}
            </AnalyticsLoader>
        </AnalyticsProvider>

      

}

