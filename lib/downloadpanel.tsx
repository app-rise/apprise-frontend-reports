import { useT } from 'apprise-frontend-core/intl/language'
import { Form } from 'apprise-ui/form/form'
import { Explainer, ExplainerPanel } from 'apprise-ui/utils/explainer'
import { CloseIcon, DownloadIcon } from 'apprise-ui/utils/icons'
import { Fragment, PropsWithChildren, useContext, useEffect, useState } from 'react'
import { AnalyticsDownloadStatsContext, AnalyticsDownloadContext } from './context'

import { useL } from 'apprise-frontend-core/intl/multilang'
import { utils } from 'apprise-frontend-core/utils/common'
import { Button } from 'apprise-ui/button/button'
import { ChoiceBox } from 'apprise-ui/choicebox/choicebox'
import { RoutableDrawerSystem, useDrawer, useRoutableDrawer } from 'apprise-ui/drawer/drawer'
import { Label } from 'apprise-ui/label/label'
import { MultiSelectBox } from 'apprise-ui/selectbox/selectbox'
import { BulkTask, BulkTaskStats, useBulktask } from 'apprise-ui/utils/bulktask'
import { DataViewIcon, analyticsType } from './constants'
import { DataviewLabel } from './label'
import { Dataview, Format, View } from './model'
import { useAnalyticsStore } from './store'
import { ViewParam, useViewParams } from './viewparams'

import { FaBarsProgress } from "react-icons/fa6"
import { IoExitOutline } from "react-icons/io5"

import { MdPause } from "react-icons/md"


import { Styled } from 'apprise-ui/component/model'
import { FaCheck } from "react-icons/fa6"
import "./downloadpanel.scss"

import { StateProvider } from 'apprise-frontend-core/state/provider'
import { useSettings } from 'apprise-frontend-streams/settings/api'
import { ReadonlyContext } from 'apprise-ui/readonly/readonly'
import { useFeedback } from 'apprise-ui/utils/feedback'
import { downloadZip } from 'client-zip'
import dateformat from 'date-fns/format'
import { MdStop } from "react-icons/md"
import streamsaver from 'streamsaver'
import { useViewClient } from './client'
import { useAnalyticsProfile } from './profile'
import { downloadParam } from './routing'
import { AnalyticsSettings } from './settings'



// the download state, including selected views and view parameter values, executon statistics, and format options.
export type DownloadState = {

    format: 'xlsx' | 'csv',
    dataviews: Dataview[]
    params: Record<string, any[]>

}

export type DownloadStatsState = {

    // these are reported as the task executes, so start off as undefined.
    stats: BulkTaskStats | undefined
}

const initialState: DownloadState = {

    format: 'xlsx',
    dataviews: [],
    params: {}
}

const initialStatsState: DownloadStatsState = {

    stats: undefined
}


export type DownloadDrawerState = RoutableDrawerSystem


//  holds state and mounts drawer in app-wide scope, so that:
//  1. any child can access it and control it.
//  2. users can return to it from anywhere. 
export const AnalyticsDownloadProvider = (props: PropsWithChildren) => {


    return <StateProvider initialState={initialStatsState} context={AnalyticsDownloadStatsContext}>
        <StateProvider initialState={initialState} context={AnalyticsDownloadContext}>
            <DrawerProvider {...props} />
        </StateProvider>
    </StateProvider >

}

export const useDownloadDrawer = () => useRoutableDrawer(downloadParam)


const DrawerProvider = (props: PropsWithChildren) => {

    const t = useT()

    const { children } = props

    const downloadKit = useDownloadDrawer()

    const { Drawer } = downloadKit

    return <Fragment>

        <Drawer translation={400} width={500} icon={<DownloadIcon />} title={t('analytics.download_title')}>
            <Panel />
        </Drawer>

        {children}
    </Fragment>
}


type TaskDefinition = BulkTask & {

    task: () => Promise<Response>
    toString: (separator?: string) => string
    dataview: Dataview
    format: Format
    view: View
    filename: string

}


// 1. collects dataview and parameters. 
// 2. launches task, shows progress, and shares progress data.  
// for 2.,  integrates with generic bulk task management api and components.
const Panel = () => {

    const t = useT()
    const fb = useFeedback()

    const drawer = useDownloadDrawer()

    const ctx = useContext(AnalyticsDownloadContext)
    const statsctx = useContext(AnalyticsDownloadStatsContext)

    const threshold = useSettings<AnalyticsSettings>(analyticsType).downloadThreshold

    const { params: paramMap, dataviews, format } = ctx.get()

    const params = useViewParams()

    // for download, use same parameters that can be locked.
    const lockableParams = params.all().filter(p => p.lockable)

    const allViews = useAnalyticsStore().allSorted().filter(params.canDownload)

    const selectedViews = dataviews.length ? dataviews : allViews.filter(r => r.predefined)

    const orderedParams = lockableParams.map(lp => lp.id)

    const { plural, singular } =  useAnalyticsProfile()

    const allFilenameComponents = [singular.toLowerCase(), ...orderedParams]

    const [filenameComponents/* , filenameComponentsSet */] = useState<string[]>([])

    const filenameComponentsOrDefault = filenameComponents.length ? filenameComponents : allFilenameComponents

    // helper: derives all download tasks from current parameter values.
    const tasks: TaskDefinition[] = useTasks(selectedViews, lockableParams, paramMap, filenameComponentsOrDefault)

    const exceedsThreshold = tasks.length > threshold


    // listen to bulk stats and shares it in context.
    const setDownloadStats = (stats: BulkTaskStats) => statsctx.set(s => s.stats = stats)

    const { Drawer: ProgressDrawer, openSet: progressOpenSet, open: progressOpen } = useDrawer()

    // integrates with bulk task management. 
    const { bulkStatus, bulkStart, bulkStop, reset, TaskList } = useBulktask<Response, TaskDefinition>({

        tasks,
        onChange: setDownloadStats,
        onDone: () => {

            if (!progressVisible)
                fb.showNotification(t('analytics.download_completed'), { icon: <DownloadIcon />, duration: false })
        },
        pool: {

            capacity: 2,
            taskOf: t => t.task()
            //, debugWithNameOf: t => t.toString()  //enable to debug.
        }

    })

    const downloading = bulkStatus === 'inprogress'

    const stopped = bulkStatus === 'stopped'

    const progressVisible = drawer.open && (downloading || progressOpen)

    // eslint-disable-next-line
    useEffect(() => reset(), [tasks.length])

    const startDownload = async () => {

        progressOpenSet(true)

        const pool = bulkStart()

        // wraps the iterable pool to prepare tasks for archiving.  
        const startedTasks = async function* gen() {

            for await (const task of pool) {

                const outcome = await task.task.catch(_ => '')

                const prefix = outcome ? '' : t('analytics.download_failure_prefix') + ' '

                const name = `${prefix + task.entry.filename}.${format}`

                yield { name: name.replaceAll("/", '-'), input: outcome }
            }
        }

        const archivename = `${t('analytics.download_archive_name_prefix', { plural })} ${dateformat(new Date(), "yyyy-MM-dd.HHmmss")}.zip`

        // archive and write out in streaming fashion
        downloadZip(startedTasks()).body?.pipeTo(streamsaver.createWriteStream(archivename))

    }

    const stopDownload = bulkStop


    const downloadBtn = <Button type='primary' enabled={!!tasks.length} disabled={exceedsThreshold} onClick={startDownload}>
        {tasks.length ? t('analytics.download_now_many_btn', { count: tasks.length, plural }) : t('analytics.download_now_btn', { plural })}
    </Button>

    const stopDownloadBtn = <Button noReadonly type='danger' enabled={!!tasks.length} onClick={stopDownload}>
        {t('analytics.download_stop_btn')}
    </Button>


    const cloProgessBtn = downloading || <Button style={{ position: 'relative', top: 5 }} size='small' type='ghost' icon={<IoExitOutline color='black' size={18} onClick={() => progressOpenSet(false)} />} noLabel />




    return <ReadonlyContext.Provider value={downloading}>

        <ExplainerPanel>

            <Explainer icon={<DownloadIcon />}>
                {t('analytics.download_explainer', { plural: plural.toLowerCase() })}
            </Explainer>

            <Form>

                <MultiSelectBox label={t('analytics.download_lbl', { plural })} msg={t('analytics.download_msg', { plural: plural.toLowerCase() })}
                    placeholderAsOption placeholder={<Label icon={<DataViewIcon />} title={t('analytics.download_placeholder', { plural: plural.toLowerCase() })} />}
                    options={allViews} keyOf={r => r?.id} render={r => <DataviewLabel bare dataview={r} />}
                    onChange={rs => ctx.set(s => s.dataviews = rs ?? [])}>
                    {ctx.get().dataviews}
                </MultiSelectBox>

                {lockableParams.map(param => {

                    const Field = param.lockField!

                    const onChange = (p: any) => ctx.set(s => s.params[param.id] = p)

                    return <Field renderMode='download' placeholder={param.initialValue?.()} label={params.nameOf(param)} className='locked-param' key={param.id} onChange={onChange}>
                        {paramMap[param.id] ?? []}
                    </Field>

                })}

                <br />

                <ChoiceBox centered msg={t('analytics.download_format_msg')} radio inline value={ctx.get().format} onChange={fmt => ctx.set(c => c.format = fmt!)}>
                    <ChoiceBox.Option value='csv' title="CSV" />
                    <ChoiceBox.Option value='xlsx' title="XLSX" />
                </ChoiceBox>

                {/* 
            <br />

            <MultiSelectBox noClear noItemRemove={filenameComponents.length == 1} label={t('analytics.download_filename_parts_lbl')} msg={t('analytics.download_filename_parts_msg', { example: filenameExample })}
                options={allFilenameComponents}
                render={renderFilenameComponent}
                onChange={rs => rs?.length && filenameComponentsSet(rs!)}>
                {filenameComponentsOrDefault}
            </MultiSelectBox> */}


            </Form>

            <br />
            <br />

            <div style={{ display: 'flex', justifyContent: 'center' }}>
                {downloading ? stopDownloadBtn : downloadBtn}
            </div>

            {exceedsThreshold &&

                <div className='threshold-warning'>
                    {t('analytics.download_threshold_disclaimer', { threshold })}
                </div>
            }

            {progressVisible &&

                <ProgressDrawer open noMask icon={<FaBarsProgress color={downloading ? 'deepskyblue' : stopped ? 'black' : undefined} />}
                    title={t(downloading ? 'analytics.download_task_title' : stopped ? 'analytics.download_task_title_stopped' : 'analytics.download_task_title_done')}
                    titleSuffix={cloProgessBtn} width={400}>
                    <div style={{ paddingTop: 15 }}>
                        <TaskList taskCard={Card} />
                    </div>
                </ProgressDrawer>
            }

        </ExplainerPanel>
    </ReadonlyContext.Provider>
}


// ------------------------------------------

// helpers, creates downnload tasks for all permutations of views and params.
const useTasks = (dataviews: Dataview[], params: ViewParam[], paramMap: Record<string, any[]>, filenameComponents: string[]) => {

    const paramdefs = useViewParams()
    const ctx = useContext(AnalyticsDownloadContext).get()
    const { singular } =  useAnalyticsProfile()
    const client = useViewClient()

    //  [ ["A", [1,2]], ["B", ["one"]] ] => [ {"A":1,"B":"one"},{"A":2,"B":"one"}]
    const permutationsOf = (entries: [string, any[]][]): TaskDefinition[] => {

        if (!entries.length)
            return []

        const [[id, values], ...rest] = entries

        const defs = permutationsOf(rest)

        const newdefs = utils().arrayOf(values).flatMap(v => defs.length ? defs.map(def => ({ ...def, [id]: v })) : { [id]: v })

        return newdefs
    }

    const defaultedParamValues = params.map(param => {

        const value = paramMap[param.id]

        const range = param.range?.() ?? []

        // defaults no/empty values to the paramer's full range. 
        const valueOrDefault = Array.isArray(value) ? (value.length ? value : range) : value ? [value] : range

        return [param.id, valueOrDefault] as [string, any[]]

    })


    // adds views to permutations.
    const tasks = permutationsOf([['dataview', dataviews], ...defaultedParamValues]).flatMap(taskdef => {

        const { dataview, ...rest } = taskdef

        const paramIds = Object.keys(rest)

        const toString = (separator: string = ' ') => params.sort(paramdefs.comparator).map(p => taskdef[p.id]).join(separator)

        const views = dataview.views.filter(view => paramIds.every(p => !view.hideOnLocked?.includes(p)))

        let filename = filenameComponents.map(c => c === singular.toLowerCase() ? dataview.name.en! : taskdef[c]).join('-')


        return views.map(view => {

            if (views.length > 1)
                filename = `${filename}-${view.tab?.en ?? view.id}`

            const task = () => client.downloadView(view, ctx.format, rest)

            return { ...taskdef, filename, format: ctx.format, view, task, toString }

        })



    })


    const sortedTasks = tasks.sort((t1, t2) => {

        const compare = t1.toString().localeCompare(t2.toString())

        return compare || t1.dataview.name.en!.localeCompare(t2.dataview.name.en!)

    })

    return sortedTasks

}


// ------------------------------------------

// cusotmise task item in bulk task list.
const Card = (props: Styled & {

    task: TaskDefinition
    taskStatus: string

}) => {

    const { task, taskStatus, ...rest } = props


    const l = useL()
    const t = useT()

    const { dataview, taskError, filename, task: download } = task

    const title = l(dataview.name)!


    const icon =
        taskStatus === 'failed' ? <CloseIcon color='orangered' /> :
            taskStatus === 'completed' ? <FaCheck color='lightseagreen' /> :
                taskStatus === 'stopped' ? <MdStop color='black' /> :
                    taskStatus === 'inprogress' ? <DownloadIcon /> :
                        <MdPause />


    const downloadAndSave = async () => {

        const outcome = await download().catch(_ => new Response(''))


        const prefix = outcome ? '' : t('analytics.download_failure_prefix') + ' '

        const name = `${prefix + filename}.${task.format}`

        outcome.body?.pipeTo(streamsaver.createWriteStream(name.replaceAll("/", '-')))

    }

    return <div {...rest}>
        <div className='card-content'>
            <div className='card-title'>
                <Label tip={taskError?.message ?? taskError} icon={icon} title={title} />
            </div>
            <div className='card-subtitle'>
                <span>{task.toString()}</span>
            </div>
        </div>
        <div className='card-btns'>
            <Button tip={t('analytics.download_task_item_dl_tip')} tipPlacement='left' type='ghost' noLabel icon={<DownloadIcon color='lightseagreen' onClick={downloadAndSave} />} />
        </div>
    </div>
}
