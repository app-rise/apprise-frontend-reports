import { State } from 'apprise-frontend-core/state/api'
import { createContext } from 'react'
import { DownloadState, DownloadStatsState } from './downloadpanel'
import { EditState } from './editprovider'
import { AnalyticsProfile, defaultAnalyticsProfile } from './profile'
import { DataviewStore, ViewStore } from './store'
import { LockedParams } from './viewparams'

// note: these contexts would be best placed next to the provider that mounts them.
// but doing so resets the context on HMR in Vite, which breaks the app and forces us to reload it.
// keep the context definitions in the same file seems to patch that.

export const ViewStoreContext = createContext<State<ViewStore>>(undefined!)

export const DataviewStoreContext = createContext<State<DataviewStore>>(undefined!)

export const AnalyticsEditContext = createContext<EditState>(undefined!)

export const LockedParamContext = createContext<State<LockedParams>>(undefined!)

export const AnalyticsDownloadContext = createContext<State<DownloadState>>(undefined!)

export const AnalyticsDownloadStatsContext = createContext<State<DownloadStatsState>>(undefined!)

export const AnalyticsProfileContext = createContext<AnalyticsProfile>(defaultAnalyticsProfile) 