import { utils } from 'apprise-frontend-core/utils/common'
import { useCrud } from 'apprise-ui/utils/crudtask'
import { useContext } from 'react'
import { useAnalyticsCalls } from './calls'
import { dataviewPlural, dataviewSingular, viewPlural, viewSingular } from './constants'
import { DataviewStoreContext, LockedParamContext, ViewStoreContext } from './context'
import { Dataview, Format, View } from './model'
import { ViewParams, useViewParams } from './viewparams'

export type ViewStore = {

    views: View[]
    stats: AnalyticsRepositoryStats
}


export type AnalyticsRepositoryStats = {

    lastRefreshed: string

    pills: JSX.Element[]

}

    & Record<string, any>

// store 
export type DataviewStore = {

    all: Dataview[]
}


export const useViewClient = () => {


    const call = useAnalyticsCalls()

    const lockedParams = useContext(LockedParamContext)

    const viewParams = useViewParams()


    const fetchView =  async (view: View, params: ViewParams = {}, encodedParams = {}) => {

        const plain = utils().index(view.params.map(viewParams.lookup).filter(p => !!p)).mappingBy(p => p!.key ?? p!.id, p => p?.initialValue?.(view) as string)

        const locked = lockedParams.get().params

        return call.fetchView(view, { plain: { ...plain, ...locked, ...params }, encoded: encodedParams })

    }


    const downloadView =  async (view: View, format: Format, params: ViewParams = {}, encodedParams = {}) => {

        const fetched = await fetchView(view, params, encodedParams)

        const response = await fetch(`/${fetched.downloadUrls[format]}`)

        if (response.status >= 300) {

            const error = await response.text()

            throw new Error(error)
        }

        return response
    }


    return { downloadView, fetchView}

}



export const useAnalyticsClient = () => {

    const dataviewstate = useContext(DataviewStoreContext)
    const viewstate = useContext(ViewStoreContext)

    const call = useAnalyticsCalls()

    const dataviewcrud = useCrud({ singular: dataviewSingular, plural: dataviewPlural })
    const viewcrud = useCrud({ singular: viewSingular, plural: viewPlural })

    const self = {


        fetchViews: viewcrud.fetchAllWith(async () => {

            const fetched = await call.fetchViews()

            viewstate.set(s => s.views = fetched)

            return fetched

        }).done()


        ,

        fetchDataviews: dataviewcrud.fetchAllWith(async () => {

            const fetched = await call.fetchDataviews()

            dataviewstate.set(s => s.all = fetched)

            return fetched

        }).done()

        ,

        save: dataviewcrud.saveOneWith(async (dataview: Dataview) => {

            const isNew = !dataview.lifecycle.created

            const saved = isNew ? await call.add(dataview) : await call.update(dataview)

            dataviewstate.set(s => s.all = isNew ? [saved, ...s.all] : s.all.map(t => t.id === saved.id ? saved : t))

            return saved

        }).with($ => $.wait(1000)).done()

        ,

        remove: dataviewcrud.removeOneWith(async (dataview: Dataview) => {

            await call.remove(dataview)

            dataviewstate.set(s => s.all = s.all.filter(c => c.id !== dataview.id))

        })
            .with(config => config.log(s => `removing ${s.id}...`))
            .withConsent()
    }

    return self
}


export const useViewLoader = () => {

    const call = useAnalyticsCalls()

    const viewcrud = useCrud({ singular: viewSingular, plural: viewPlural })

    const lockedParams = useContext(LockedParamContext)

    const viewParams = useViewParams()

    const fetchViewSilently = async (view: View, params: ViewParams = {}, encodedParams = {}) => {

        const plain = utils().index(view.params.map(viewParams.lookup).filter(p => !!p)).mappingBy(p => p!.key ?? p!.id, p => p?.initialValue?.(view) as string)

        const locked = lockedParams.get().params

        return call.fetchView(view, { plain: { ...plain, ...locked, ...params }, encoded: encodedParams })

    }

    const fetchView = viewcrud.fetchOneWith(async (view: View, params: ViewParams = {}, encodedParams = {}) => {

        return fetchViewSilently(view, params, encodedParams)

    }).done()


    return { fetchView, fetchViewSilently }

}