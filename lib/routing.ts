import { useRouting } from 'apprise-ui/utils/routing'
import { useAnalyticsProfile } from './profile'


export const newParam = "new"
export const editParam = 'edit'
export const lockParam = 'lock'
export const viewParam = 'view'
export const downloadParam = 'download'

export const useAnalyticsRouting = () => {

    const routing = useRouting()

    const { route } = useAnalyticsProfile()

    const self = {

        listRoute: () => route,

        detailRoute: (id: string) => `${route}/${id}`,

        routeToList: () => routing.routeTo(route),

        routeToDetail: (id: string) => routing.routeTo(self.detailRoute(id))

    }


    return self
}