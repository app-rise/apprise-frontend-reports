import { useL } from 'apprise-frontend-core/intl/multilang'
import { utils } from 'apprise-frontend-core/utils/common'
import { useCrud } from 'apprise-ui/utils/crudtask'
import fastsort from 'fast-sort'
import { useContext } from 'react'
import { useAnalyticsCalls } from './calls'
import { viewPlural, viewSingular } from './constants'
import { DataviewStoreContext, LockedParamContext, ViewStoreContext } from './context'
import { Dataview, View, useDataviewModel } from './model'
import { useAnalyticsProfile } from './profile'
import { ViewParams, useViewParams } from './viewparams'

export type ViewStore = {

    views: View[]
    stats: AnalyticsRepositoryStats
}


export type AnalyticsRepositoryStats = {

    lastRefreshed: string

    pills: JSX.Element[]

}

    & Record<string, any>

// store 
export type DataviewStore = {

    all: Dataview[]
}




export const useAnalyticsStore = () => {

    const l = useL()

    const dataviewstate = useContext(DataviewStoreContext)
    const viewstate = useContext(ViewStoreContext)

    const model = useDataviewModel()
    const profile = useAnalyticsProfile()

    const self = {


        all: () => dataviewstate.get().all?.filter( v => v.group === profile.group)

        ,

        allSorted: () => fastsort([...self.all()]).by([{ desc: r => r.predefined }, { asc: r => l(r.name) }])

        ,

        allViews: () => viewstate.get().views

        ,

        allViewsSorted: () => fastsort([...self.allViews()]).by([{ asc: r => r.name }])

        ,

        lookup: (id: string) => self.all().find(s => s.id === id)

        ,


        safeLookup: (id: string) => self.lookup(id) ?? model.noDataview(id)


        ,

        lookupView: (id: string) => self.allViews().find(v => v.id === id)
    
        ,
    }

    return self
}


export const useViewLoader = () => {

    const call = useAnalyticsCalls()

    const viewcrud = useCrud({ singular: viewSingular, plural: viewPlural })

    const lockedParams = useContext(LockedParamContext)

    const viewParams = useViewParams()

    const fetchViewSilently = async (view: View, params: ViewParams = {}, encodedParams = {}) => {

        const plain = utils().index(view.params.map(viewParams.lookup).filter(p => !!p)).mappingBy(p => p!.key ?? p!.id, p => p?.initialValue?.(view) as string)

        const locked = lockedParams.get().params

        return call.fetchView(view, { plain: { ...plain, ...locked, ...params }, encoded: encodedParams })

    }

    const fetchView = viewcrud.fetchOneWith(async (view: View, params: ViewParams = {}, encodedParams = {}) => {

        return fetchViewSilently(view, params, encodedParams)

    }).done()


    return { fetchView, fetchViewSilently }

}