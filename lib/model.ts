import { useT } from 'apprise-frontend-core/intl/language'
import { Multilang, useL } from 'apprise-frontend-core/intl/multilang'
import { utils } from 'apprise-frontend-core/utils/common'
import { Lifecycle } from 'apprise-frontend-core/utils/lifecycle'
import { Note } from 'apprise-frontend-iam/user/notebox'
import { ViewParam } from './viewparams'

export type ViewDescriptor = {

    type: 'question' | 'dashboard'
    id: string
    name: string
}

export type View = ViewDescriptor & ViewUrls & {

    tab?: Multilang,
    hideOnLocked?: string[]
    params: (ViewParam['id'])[]

}


export type Format = string

export type ViewUrls = {

    embedUrl: string
    downloadUrls: Map<Format, string>
}


export type Dataview = {

    id: string
    name: Multilang
    description: Multilang

    group: string | undefined,

    landingDetail: boolean,

    predefined: boolean

    lifecycle: Lifecycle<'active' | 'inactive'>

    views: View[]

    note: Note

}

export const newDataview = (id: string = utils().mint('DW'), group: string | undefined): Dataview => ({

    id,
    name: { en: undefined },
    landingDetail: false,
    group,
    description: { en: undefined },
    predefined: true,
    note: {},
    lifecycle: { state: 'active' },
    views: []


})


export const useDataviewModel = () => {

    const l = useL()
    const t = useT()

    const self = {


        newDataview

        ,

        noDataview: (id: string): Dataview => ({ ...newDataview('unknown',undefined), name: { en: id }, lifecycle: { state: 'inactive' }, })

        ,

        isNoDataview: (report: Dataview) => report.id === 'unknown'

        ,

        textOf: (report: Dataview) => `${l(report.name)} ${l(report.description)} ${report.predefined ? t('report.predefined') : ''}`


    }

    return self
}


export const useViewModel = () => {


    const self = {


        textOf: (view: View) => view.name


    }

    return self
}