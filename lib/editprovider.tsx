
import { useT } from 'apprise-frontend-core/intl/language'
import { useForm } from 'apprise-frontend-core/utils/form'
import { Button } from 'apprise-ui/button/button'
import { useRoutableDrawer } from 'apprise-ui/drawer/drawer'
import { AddIcon, EditIcon } from 'apprise-ui/utils/icons'
import { PropsWithChildren, useEffect } from 'react'
import { AnalyticsEditContext } from './context'
import { useEditorFields } from './fields'
import { Dataview, useDataviewModel } from './model'
import { useAnalyticsOracle } from './oracle'
import { useAnalyticsProfile } from './profile'
import { editParam, lockParam, newParam } from './routing'
import { useAnalyticsStore } from './store'




export const EditStateProvider = (props: PropsWithChildren) => {

    const { children } = props

    const value = useEditState()

    return <AnalyticsEditContext.Provider value={value}>
        {children}
    </AnalyticsEditContext.Provider >
}

// split provider and hook, so that we can type the context using the hook's return value.

export type EditState = ReturnType<typeof useEditState>


const useEditState = () => {

    const t = useT()

    const model = useDataviewModel()
    const store = useAnalyticsStore()

    const oracle = useAnalyticsOracle()

    const editkit = useRoutableDrawer(editParam)
    const lockkit = useRoutableDrawer(lockParam)

    const { singular, group } = useAnalyticsProfile()

    const dataviewId = editkit.match

    const isNew = dataviewId === newParam

    const dataview = isNew ? model.newDataview(undefined, group) : store.safeLookup(dataviewId)

    const form = useForm(dataview)

    // eslint-disable-next-line
    useEffect(form.reset.to(dataview).quietly, [dataviewId])

    const fields = useEditorFields(form)

    const errors = fields.errors()


    const addBtn =  oracle.canManageAll() && < Button icon={<AddIcon />} onClick={() => editkit.openAt(newParam)}>
        {t("analytics.add_new", { singular })}
    </ Button>

    const editBtn = (detail: Dataview) => oracle.canEdit(detail) && <Button type="primary" icon={<EditIcon />} onClick={() => editkit.openAt(detail.id)}>
        {t("analytics.edit", { singular })}
    </ Button>


    return { ...form, isNew, editkit, lockkit, fields, errors, addBtn, editBtn }


}


