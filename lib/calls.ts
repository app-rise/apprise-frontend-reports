// import { useCall } from 'apprise-frontend-core/client/call'
import { useCall } from 'apprise-frontend-core/client/call'
import { usePreload } from 'apprise-frontend-core/client/preload'
import { Dataview, View, ViewDescriptor, ViewUrls } from './model'


export const dataviewservice = "analytics"
export const dataviewapi = "/analytics"
export const viewapi = `${dataviewapi}/view`


export type ViewParams = Partial<{

    plain: Record<string,string>
    encoded: Record<string,string>
}>

export const useAnalyticsCalls = () => {

    const call = useCall();

    const preload = usePreload()
    
    const self = {

        
        fetchDataviews: () =>  preload.get<Dataview[]>(dataviewapi) ?? call.at(dataviewapi,dataviewservice).get<Dataview[]>()

        ,

        add: (dataview:Dataview) => call.at(dataviewapi,dataviewservice).post<Dataview>(dataview) 
        ,

        update: (dataview:Dataview) => call.at(`${dataviewapi}/${dataview.id}`,dataviewservice).put<Dataview>(dataview) 

        ,

        remove: (dataview:Dataview) => call.at(`${dataviewapi}/${dataview.id}`,dataviewservice).delete() 
        
        ,

        fetchViews: () => preload.get<View[]>(viewapi) ??  call.at(viewapi,dataviewservice).get<ViewDescriptor[]>()

        ,

        fetchView: (view:View, params?: ViewParams) => call.at(viewapi,dataviewservice).post<ViewUrls>({view,params}) 
    }

    return self
}