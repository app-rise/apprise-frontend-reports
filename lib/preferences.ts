

export type AnayticsPreferencs = {

    analytics: {
        displayMode: 'full' | 'compact'
    }

}