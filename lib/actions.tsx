import { Action } from 'apprise-frontend-core/authz/action'
import { any } from 'apprise-frontend-core/authz/constants'
import { useTenancyOracle } from 'apprise-frontend-iam/authz/tenant'
import { PermissionBox, PermissionBoxProps } from 'apprise-frontend-iam/permission/permissionbox'
import { UserPermissionModule } from 'apprise-frontend-iam/user/modules'
import { DataViewIcon, DataviewSectionIcon, dataviewPlural, dataviewSingular, analyticsType } from './constants'
import { Dataview } from './model'


const base = { type: analyticsType, actionType:'admin' as const,  icon: <DataViewIcon /> , resource : any}


export const reportActions: Record<string,Action> = {

    manage : { ...base, labels: ["manage"], shortName: "analytics.actions.manage_short", name: "analytics.actions.manage_name", description: "analytics.actions.manage_desc" }

}



export const useReportPermissionModule = () => {

    
    return {

        Icon: DataviewSectionIcon,
        type: analyticsType,
        plural: dataviewPlural,
        maxPrivilege: reportActions.manage,

        box: ReportPermissionBox,

       
    } as UserPermissionModule<Dataview>


}


export const ReportPermissionBox = (props: Partial<PermissionBoxProps<Dataview>>) => {

    const oracle = useTenancyOracle()


    const { ...rest } = props

    // not visible to a tenant user.
    if (oracle.isTenantUser())
        return undefined

    return <PermissionBox<Dataview>

        wildcard preselectWildcard

        resourceSingular={dataviewSingular}
        resourcePlural={dataviewPlural}

        permissionRange={Object.values(reportActions)}
        maxPrivilege={reportActions.manage}


        {...rest}

    />

}